const fs = require("fs");

function promiseGetIpRanges(file) {
    return new Promise((resolve, reject) => {
        fs.readFile(file, (err, data) => {
            if (err) reject(err);

            let ips = data.toString();
            let ranges = parseIpRanges(ips);
            ranges = normalizeRanges(ranges);

            ranges = ranges.join(',')

            resolve(ranges);
        });
    });
}


function parseIpRanges(ips) {
    let pattern = /\d+\.\d+\.\d+\.\d+-\d+\.\d+\.\d+\.\d+/gm;
    let ranges = ips.match(pattern);

    ranges = ranges.map(e => {
        return e.split('-')
    });

    return ranges;
}

function normalizeRanges(ranges) {
    return ranges.map(function(range) {
        let [ min, max ] = [range[0], range[1]];
        let result = '';

        min = min.split('.');
        max = max.split('.');

        for (let i = 0; i < min.length; i++) {
            result += (max[i] > min[i]) ? `${min[i]}-${max[i]}` : min[i];
            result += (i !== min.length - 1) ? '.' : '';
        }

        return result
    });
}

module.exports = promiseGetIpRanges;
