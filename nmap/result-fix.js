const fs = require('fs');

let file = './result-scan.txt'

fs.readFile(file, (err, data) => {
  if (err) return console.log(err)

  data = data.toString();

  let ips = data.match(/(\d+\.){3}\d+/gm)
  ips = getUnique(ips).join('\r\n')
  fs.writeFile('./result-fix.txt', ips, (err, data) => {
    if (err) return console.log(err)

    console.log('done')
  });
});


let getUnique = function (arr) {
    var i = 0,
    current,
    length = arr.length,
    unique = [];
    for (; i < length; i++) {
      current = arr[i];
      if (!~unique.indexOf(current)) {
        unique.push(current);
      }
    }
    return unique;
  };
