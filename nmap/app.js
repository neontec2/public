const fs = require('fs');
const rangesRow = require('./ipRangesNormalize');

let file = 'source-ip-ranges.txt'

rangesRow(file).then(function(ips) {
    ips = ips.split(',').join('\r\n')

    fs.writeFile('./normalized-ips.txt', ips, 'utf-8', (err, data) => {
        if (err) {
            return console.log(err);
        }

        console.log('Done -> Ip ranges transform to nmap view')
    });
});
